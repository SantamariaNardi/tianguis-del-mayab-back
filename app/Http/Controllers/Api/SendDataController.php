<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Producer;
use App\Models\Product;
use App\Models\Event;


class SendDataController extends Controller
{
    //
    public function getCategories()
    {
        $category = Category::all();
        return response()->json(['data' => $category]);
    }
    public function getProducts()
    {
        $p = Product::all();
        $c = Category::all();
        return response()->json(['data' => $p, 'categories' => $c]);
    }
    public function getProducers()
    {
        $p = Producer::all();
        return response()->json(['data' => $p]);
    }
    public function getEvents(){
        $events = Event::all();
        return response()->json(['data' => $events]);
    }
    public function getProductsByProducer($id){
        $items = Product::where('producer_id', $id)->get();
        return response()->json(['data' => $items]);
    }
    public function getProductsByCategory($id){
        $items = Product::where('category_id', $id)->get();
        return response()->json(['data' => $items]);
    }
    public function getProduct($id){
        $item = Product::where('id', $id)->first();
        $p = Producer::where('id', $item->producer_id)->first();
        return response()->json(['data' => $item, 'producer' => $p]);
    } 
    public function getProducer($id){
        $p = Producer::where('id', $id)->first();
        return response()->json(['data' => $p]);
    } 
    public function getCategory($id){
        $p = Category::where('id', $id)->first();
        return response()->json(['data' => $p]);
    } 
      
}
