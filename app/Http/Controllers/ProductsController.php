<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use App\Models\Producer;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with([
            'products'=> $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create')->with([
            'producers'=> Producer::all(),
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $product = new Product;
            $product->name = $request->name;
            $product->description = $request->description;
            $product->status = $request->status;
            $product->category_id = $request->category_id;
            $product->producer_id = $request->producer_id;
            $product->price = $request->price;
            $product->img = $request->img;
            $product->save();
            return redirect('/products-index')->with('message', 'Producto añadido');
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect('/products-index')->with('error','Error al guardar');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $product = Product::find($id);
            $product->name = $request->name;
            $product->description = $request->description;
            $product->status = $request->status;
            $product->category_id = $request->category_id;
            $product->producer_id = $request->producer_id;
            $product->price = $request->price;
            $product->img = "C:\Users\nardi\source\tianguis-del-mayab-back\public\assets\img".$request->img;
            $product->save();
            return redirect('/products-index')->with('message', 'Producto actualizado');
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect('/products-index')->with('error','Error al actualizar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $p = Product::find($id);
            $p->delete();
            return redirect('/products-index')->with('message','Producto eliminado');;
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect('/products-index')->with('error','Este productor aún tiene productos');
        }
    }

    public function changeStatus($id){
        try{
            $p=Product::find($id);
            if($p->status == 1)
                $p->update(['status' => '0']);
            elseif($p->status == 0 )
                $p->update(['status' => '1']);
            return redirect('/products-index')->with('message','El estatus del producto ha cambiado');;
        }
        catch(Throwable $e){
            return Redirect('/products-index')->with('error','No funcionó');
        }
    }
}
