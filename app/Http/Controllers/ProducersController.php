<?php

namespace App\Http\Controllers;

use App\Models\Producer;
use Illuminate\Http\Request;
use Redirect;

class ProducersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $producers = Producer::all();
        return view('producers.index')->with([
            'producers'=> $producers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('producers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $producer = new Producer;
            $producer->name = $request->name;
            $producer->history = $request->history;
            $producer->img = $request->img;
            $producer->code = $request->code;
            $producer->city = $request->city;
            $producer->address = $request->address;
            $producer->phone = $request->phone;
            $producer->user_id = $request->user_id;
            $producer->status = $request->status;
            $producer->initial_date = $request->initial_date;
            $producer->end_date = $request->end_date;
            $producer->facebook_link = $request->facebook_link;
            $producer->whatsapp_link = $request->whatsapp_link;
            $producer->save();
            //return Redirect::back()->with('msg', 'The Message');
            return Redirect('/producers-index')->with('message','Productor añadido con exito!');
        }
        catch(\Illuminate\Database\QueryException $e){
            return redirect::back()->with('error','No se pudo guardar');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producer  $producer
     * @return \Illuminate\Http\Response
     */
    public function show(Producer $producer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producer  $producer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('producers.create')->with([
            'producers'=> Producer::where('id', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producer  $producer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $producer = Producer::find($id);
            $producer->name = $request->name;
            $producer->img = $request->img;
            $producer->code = $request->code;
            $producer->city = $request->city;
            $producer->address = $request->address;
            $producer->phone = $request->phone;
            $producer->user_id = $request->user_id;
            $producer->status = $request->status;
            $producer->initial_date = $request->initial_date;
            $producer->end_date = $request->end_date;
            $producer->facebook_link = $request->facebook_link;
            $producer->whatsapp_link = $request->whatsapp_link;
            $producer->save();
            return Redirect('/producers-index')->with('message','Productor actualizado');
        }
        catch(\Illuminate\Database\QueryException $e){
            return Redirect('/producers-index')->with('error','No se pudo actualizar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producer  $producer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $p = Producer::find($id);
            $p->delete();
            return Redirect('/producers-index')->with('message','Productor eliminado');
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect('/producers-index')->with('error','Este productor aún tiene productos');
        }
    }
}
