<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('categories.index')->with([
            'categories'=> $category
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $category = new Category;
            $category->name = $request->name;
            $category->description = $request->description;
            $category->status = $request->status;
            $category->img = $request->img;
            $category->save();

            return redirect('/categories-index');
        }
        catch(\Illuminate\Database\QueryException $e){
            return redirect('/categories-index')->with('error', 'Error al guardar');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $category = Category::find($id);
            $category->name = $request->name;
            $category->description = $request->description;
            $category->status = $request->status;
            $category->img = $request->img;
            $category->save();
        }
        catch(\Illuminate\Database\QueryException $e){
            return redirect('/categories-index')->with('error', 'Error al actualizar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $category = Category::find($id);
            $category->delete();
            return redirect('/categories-index')->with('message', 'Categoria eliminada');
        }
        catch(\Illuminate\Database\QueryException $e){
            return redirect('/categories-index')->with('error', 'Esta categoria tiene aún productos');
        }
    }
}
