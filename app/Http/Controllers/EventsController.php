<?php

namespace App\Http\Controllers;
use App\Models\Event;
use Illuminate\Http\Request;
use Redirect;
class EventsController extends Controller
{
    //
    public function index()
    {
        $events = Event::all();
        return view('events.index')->with([
            'events'=> $events
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event;
       /*  $event->name = $request->name;
        $event->description = $request->description;
        $event->status = $request->status; */
        $event->img = $request->img;
        $event->save();

        return redirect::back()->with('message', 'Imagen de evento añadida');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);
        $event->name = $request->name;
        $event->description = $request->description;
        $event->status = $request->status;
        $event->img = $request->img;
        $event->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();
        return redirect::back()->with('message', 'Imagen de evento eliminada');
    }

    public function changeStatus($id){
        try{
            $ev=Event::find($id);
            if($ev->status == 1){
                $ev->update(['status' => '0']);
            }
            elseif($ev->status == 0 ){
                $ev->update(['status' => '1']);
            }
            return redirect('/events-index')->with('message','El estatus del evento ha cambiado');
        }
        catch(Throwable $e){
            return Redirect('/events-index')->with('error','No funcionó');
        }
    }
}
