<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producer extends Model
{
    use HasFactory;
    protected $table = 'producers';
    protected $fillable = [
        'name','history','img','code','status','initial_date','end_date', 'whatsapp_link', 'facebook_link'
    ];
}
