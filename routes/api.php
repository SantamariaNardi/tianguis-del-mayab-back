<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(array('namespace' => 'Api'), function () {
    Route::get('/getCategories', [App\Http\Controllers\Api\SendDataController::class, 'getCategories']);
    Route::get('/getCategory/{id}', [App\Http\Controllers\Api\SendDataController::class, 'getCategory']);

    Route::get('/getProducts', [App\Http\Controllers\Api\SendDataController::class, 'getProducts']);
    Route::get('/getProducers', [App\Http\Controllers\Api\SendDataController::class, 'getProducers']);
    Route::get('/getProductsByProducer/{id}', [App\Http\Controllers\Api\SendDataController::class, 'getProductsByProducer']);
    Route::get('/getProductsByCategory/{id}', [App\Http\Controllers\Api\SendDataController::class, 'getProductsByCategory']);
    Route::get('/getProduct/{id}', [App\Http\Controllers\Api\SendDataController::class, 'getProduct']);
    Route::get('/getProducer/{id}', [App\Http\Controllers\Api\SendDataController::class, 'getProducer']);
    Route::get('/getEvents', [App\Http\Controllers\Api\SendDataController::class, 'getEvents']);
});
