<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
})->name('login');


Auth::routes();



Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
    Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');
    Route::get('/edit-user', [App\Http\Controllers\ProfileController::class, 'edit'])->name('edit-user');
    //users
    Route::get('/users-index', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');

    //categories
    Route::get('/categories-index', [App\Http\Controllers\CategoryController::class, 'index'])->name('categories.index');
    Route::get('/categories-create', [App\Http\Controllers\CategoryController::class, 'create'])->name('categories.create');
    Route::post('/categories-store', [App\Http\Controllers\CategoryController::class, 'store'])->name('categories.store');
    Route::delete('/categories-delete/{id}', [App\Http\Controllers\CategoryController::class, 'destroy'])->name('categories.delete');
    Route::post('/categories-update/{id}', [App\Http\Controllers\CategoryController::class, 'update'])->name('categories.update');
    
    //producers
    Route::get('/producers-index', [App\Http\Controllers\ProducersController::class, 'index'])->name('producers.index');
    Route::get('/producers-create', [App\Http\Controllers\ProducersController::class, 'create'])->name('producers.create');
    Route::post('/producers-store', [App\Http\Controllers\ProducersController::class, 'store'])->name('producers.store');
    Route::delete('/producers-delete/{id}', [App\Http\Controllers\ProducersController::class, 'destroy'])->name('producers.delete');
    Route::post('/producers-update/{id}', [App\Http\Controllers\ProducersController::class, 'update'])->name('producers.update');

    //products
    Route::get('/products-index', [App\Http\Controllers\ProductsController::class, 'index'])->name('products.index');
    Route::get('/products-create', [App\Http\Controllers\ProductsController::class, 'create'])->name('products.create');
    Route::post('/products-store', [App\Http\Controllers\ProductsController::class, 'store'])->name('products.store');
    Route::delete('/products-delete/{id}', [App\Http\Controllers\ProductsController::class, 'destroy'])->name('products.delete');
    Route::post('/products-update/{id}', [App\Http\Controllers\ProductsController::class, 'update'])->name('products.update');
    Route::put('/products-changeStatus/{id}', [App\Http\Controllers\ProductsController::class, 'changeStatus'])->name('products.changeStatus');

    //events
    Route::get('/events-index', [App\Http\Controllers\EventsController::class, 'index'])->name('events.index');
   //Route::get('/events-create', [App\Http\Controllers\EventsController::class, 'create'])->name('events.create');
    Route::post('/events-store', [App\Http\Controllers\EventsController::class, 'store'])->name('events.store');
    Route::delete('/events-delete/{id}', [App\Http\Controllers\EventsController::class, 'destroy'])->name('events.delete');
    Route::put('/events-changeStatus/{id}', [App\Http\Controllers\EventsController::class, 'changeStatus'])->name('events.changeStatus');

});