@extends('layouts.app', ['activePage' => 'categorias', 'title' => 'Light Bootstrap Dashboard Laravel by Creative Tim & UPDIVISION', 'navName' => 'Table List', 'activeButton' => 'laravel'])
@section('content')
<div class="content">
        <div class="container-fluid">
        <!-- aqui emmpiezas a meter código -->
            <div class="row">
                <div class="container-fluid">
                    <div class="card">
                        <h5 class="text-center mb-4">Agregar una categoria</h5>
                            <div class="container">
                                <form class="form-card" method="POST" action="{{ route('categories.store') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="">Nombre </label>
                                        <input type="text" class="form-control" name="name" id="" aria-describedby="name" placeholder="Nombre completo">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Descripción</label>
                                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    
                                    <div class="col-md-13 row p-0">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputPassword1">Imagen</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="img" id="customFileLang" lang="es">
                                                <label class="custom-file-label mb-0" for="customFileLang">Seleccionar Imagen</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="" for="exampleCheck1">Estatus</label>
                                            <select class="custom-select" name="status">
                                                <option selected>Seleccionar</option>
                                                <option value="1">Activo</option>
                                                <option value="2">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="/categories-index" class="btn btn-secondary">cancelar</a>
                                </form>
                            </div>  
                    </div>
                </div>
            </div>
            <div class="row">
                
            </div>
        </div>
    </div>
    <style>
        .custom-file:(
            en: "Browse",
            es: "Elegir"
        );
    </style>
@endsection