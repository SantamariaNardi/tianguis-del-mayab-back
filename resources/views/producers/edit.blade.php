@extends('layouts.app', ['activePage' => 'producers', 'title' => 'Light Bootstrap Dashboard Laravel by Creative Tim & UPDIVISION', 'navName' => 'Table List', 'activeButton' => 'laravel'])
@section('content')
<div class="content">
        <div class="container-fluid">
        <!-- aqui emmpiezas a meter código -->
            <div class="row">
                <div class="container-fluid">
                    <div class="card">
                        <h5 class="text-center mb-4">Agregar una productor</h5>
                            <div class="container">
                                <form class="form-card" method="POST" action="{{ route('producers.store') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="">Nombre </label>
                                        <input value="" type="text" class="form-control" name="name" id="" aria-describedby="name" placeholder="Nombre completo">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Historia del productor</label>
                                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Código </label>
                                        <input value="" type="text" class="form-control" name="code" id="" aria-describedby="name" placeholder="código unico">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Ciudad </label>
                                        <input value="" type="text" class="form-control" name="city" id="" aria-describedby="name" placeholder="Ciudad">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Dirección </label>
                                        <input value="" type="text" class="form-control" name="address" id="" aria-describedby="name" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Telefono</label>
                                        <input value="" type="text" class="form-control" name="phone" id="" aria-describedby="name" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Fecha de inicio</label>
                                        <input value="" type="date" class="form-control" name="initial_date" id="" aria-describedby="name" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Fecha de terminación</label>
                                        <input value="" type="date" class="form-control" name="end_date" id="" aria-describedby="name" placeholder="">
                                    </div>
                                    
                                    <div class="col-md-13 row p-0">
                                        
                                        <div class="form-group col-md-6">
                                            <label class="" for="exampleCheck1">Estatus</label>
                                            <select class="custom-select" name="status">
                                                <option selected>Seleccionar</option>
                                                <option value="1">Activo</option>
                                                <option value="2">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="/producers-index" class="btn btn-secondary">cancelar</a>
                                </form>
                            </div>  
                    </div>
                </div>
            </div>
            <div class="row">
                
            </div>
        </div>
    </div>
    <style>
        .custom-file:(
            en: "Browse",
            es: "Elegir"
        );
    </style>
@endsection