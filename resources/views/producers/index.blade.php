@extends('layouts.app', ['activePage' => 'producers', 'title' => 'Tianguis del Mayab', 'navName' => 'Table List', 'activeButton' => 'laravel'])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card"> 
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h4 class="card-title"> Productores</h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('producers.create') }}" class="btn btn-sm btn-default"><i class="fa fa-plus" aria-hidden="true"></i>Nuevo productor</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-primary">
                                    <th>
                                        Nombre
                                    </th>
                                    <th style="width: 200px">
                                        Historia del productor
                                    </th>
                                    <th>
                                        Imagen
                                    </th>
                                    <th>
                                        Código
                                    </th>
                                    <th>
                                        Ciudad
                                    </th>
                                    <th>
                                        Dirección
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        User_id
                                    </th>
                                    <th>
                                        Estatus
                                    </th>
                                    <th>
                                        Fecha de inicio
                                    </th>
                                    <th>
                                        Fecha de terminación
                                    </th>
                                    <th>
                                        Opciones
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach($producers as $item)
                                    <tr>
                                        <td>
                                            {{ $item->name }}
                                        </td>
                                        <td style="width: 200px">
                                            {{ $item->history }}
                                        </td>
                                        <td>
                                            <img src="assets/img/{{ $item->img }}" class="rounded" alt="Picture" width="100" height="100"> 
                                        </td>
                                        <td>
                                            {{ $item->code }}
                                        </td>
                                        <td>
                                            {{ $item->city }}
                                        </td>
                                        <td>
                                            {{ $item->address }}
                                        </td>
                                        <td>
                                            {{ $item->phone }}
                                        </td>
                                        <td>
                                            {{ $item->user_id }}
                                        </td>
                                        <td>
                                            {{ $item->status }}
                                        </td>
                                        <td>
                                            {{ $item->initial_date }}
                                        </td>
                                        <td>
                                            {{ $item->end_date }}
                                        </td>
                                        <td>
                                            <a class="nav-link" style="color:gray" href="" >
                                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                            </a>
                                            <form method="POST" action="{{ route('producers.delete', [$item->id]) }}" >
                                            @csrf
                                            @method('DELETE')
                                                <button class="btn" style="background-color:transparent color:gray" type="submit">
                                                    <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
           <!--  <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header">
                        <h4 class="card-title"> Table on Plain Background</h4>
                        <p class="card-category"> Here is a subtitle for this table</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Country
                                    </th>
                                    <th>
                                        City
                                    </th>
                                    <th class="text-right">
                                        Salary
                                    </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Dakota Rice
                                        </td>
                                        <td>
                                            Niger
                                        </td>
                                        <td>
                                            Oud-Turnhout
                                        </td>
                                        <td class="text-right">
                                            $36,738
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minerva Hooper
                                        </td>
                                        <td>
                                            Curaçao
                                        </td>
                                        <td>
                                            Sinaai-Waas
                                        </td>
                                        <td class="text-right">
                                            $23,789
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sage Rodriguez
                                        </td>
                                        <td>
                                            Netherlands
                                        </td>
                                        <td>
                                            Baileux
                                        </td>
                                        <td class="text-right">
                                            $56,142
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Philip Chaney
                                        </td>
                                        <td>
                                            Korea, South
                                        </td>
                                        <td>
                                            Overland Park
                                        </td>
                                        <td class="text-right">
                                            $38,735
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Doris Greene
                                        </td>
                                        <td>
                                            Malawi
                                        </td>
                                        <td>
                                            Feldkirchen in Kärnten
                                        </td>
                                        <td class="text-right">
                                            $63,542
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mason Porter
                                        </td>
                                        <td>
                                            Chile
                                        </td>
                                        <td>
                                            Gloucester
                                        </td>
                                        <td class="text-right">
                                            $78,615
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Jon Porter
                                        </td>
                                        <td>
                                            Portugal
                                        </td>
                                        <td>
                                            Gloucester
                                        </td>
                                        <td class="text-right">
                                            $98,615
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
@endsection