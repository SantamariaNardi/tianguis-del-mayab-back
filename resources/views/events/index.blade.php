@extends('layouts.app', ['activePage' => 'events', 'title' => 'Tianguis del Mayab', 'navName' => 'Table List', 'activeButton' => 'laravel'])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Eventos</h4>
                        <p class="card-category"> Añade una imagen con información del próximo Tianguis del Mayab</p>
                    </div>
                    <div class="card-body">
                        <form class="form-card pl-3" method="POST" action="{{ route('events.store') }}">
                            @csrf
                            <label >Añadir nueva imagen</label>
                            <div class="row col-md-12">
                                <input type="file" name="img" class="form-control col-md-6">&nbsp&nbsp
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        Imagen
                                    </th>
                                </thead>
                                <tbody>
                                    @if($events)
                                        @foreach($events as $event)
                                            <tr>
                                                <td>
                                                    <img src="assets/img/{{ $event->img }}" class="rounded" alt="Picture" height="150" > 
                                                </td>
                                                <td>
                                                <form method="POST" action="{{ route('events.changeStatus', [$event->id]) }}" >
                                                    @csrf
                                                    @method('PUT')
                                                    @if ( $event->status == 1 )
                                                        <button type="submit" style="color:white; border:none" class="badge bg-success bg-gradient-success">Activo</button>
                                                    @else
                                                        <button style="color:white; border:none" type="submit" class="badge bg-secondary bg-gradient-secondary">Inactivo</button>
                                                    @endif   
                                                </form>
                                                </td>
                                                <td style="display:revert">
                                                    <form method="POST" action="{{ route('events.delete', [$event->id]) }}" >
                                                    @csrf
                                                    @method('DELETE')
                                                        <button class="btn" style="background-color:transparent color:gray" type="submit">
                                                            <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection