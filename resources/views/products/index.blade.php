@extends('layouts.app', ['activePage' => 'products', 'title' => 'Tianguis del Mayab', 'navName' => 'Table List', 'activeButton' => 'laravel'])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h4 class="card-title"> Productos</h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('products.create') }}" class="btn btn-sm btn-default"><i class="fa fa-plus" aria-hidden="true"></i>Nuevo producto</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-primary">
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Descripción
                                    </th>
                                    <th>
                                        Categoria
                                    </th>
                                    <th>
                                        Productor
                                    </th>
                                    <th>
                                        Estatus
                                    </th>
                                    <th>
                                        Imagen
                                    </th>
                                    <th>
                                        Precio
                                    </th>
                                    <th>
                                        Opciones
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach($products as $item)
                                    <tr>
                                        <td>
                                            {{ $item->name }}
                                        </td>
                                        <td>
                                            {{ $item->description }}
                                        </td>
                                        <td>
                                            {{ $item->category_id }}
                                        </td>
                                        <td>
                                            {{ $item->producer_id }}
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('products.changeStatus', [$item->id]) }}" >
                                                @csrf
                                                @method('PUT')
                                                @if ( $item->status == 1 )
                                                    <button type="submit" style="color:white; border:none" class="badge bg-success bg-gradient-success">Activo</button>
                                                @else
                                                    <button style="color:white; border:none" type="submit" class="badge bg-secondary bg-gradient-secondary">Inactivo</button>
                                                @endif   
                                            </form>
                                        </td>
                                        <td>
                                            <img src="assets/img/{{ $item->img }}" class="rounded" alt="Picture" width="100" height="100"> 
                                        </td>
                                        <td>
                                            ${{ $item->price }}
                                        </td>
                                        <td>
                                           <!--  <a class="nav-link" style="color:gray" href="" >
                                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                            </a> -->
                                            <form method="POST" action="{{ route('products.delete', [$item->id]) }}" >
                                            @csrf
                                            @method('DELETE')
                                                <button class="btn" style="background-color:transparent color:gray" type="submit">
                                                    <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
           <!--  <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header">
                        <h4 class="card-title"> Table on Plain Background</h4>
                        <p class="card-category"> Here is a subtitle for this table</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Country
                                    </th>
                                    <th>
                                        City
                                    </th>
                                    <th class="text-right">
                                        Salary
                                    </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Dakota Rice
                                        </td>
                                        <td>
                                            Niger
                                        </td>
                                        <td>
                                            Oud-Turnhout
                                        </td>
                                        <td class="text-right">
                                            $36,738
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minerva Hooper
                                        </td>
                                        <td>
                                            Curaçao
                                        </td>
                                        <td>
                                            Sinaai-Waas
                                        </td>
                                        <td class="text-right">
                                            $23,789
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sage Rodriguez
                                        </td>
                                        <td>
                                            Netherlands
                                        </td>
                                        <td>
                                            Baileux
                                        </td>
                                        <td class="text-right">
                                            $56,142
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Philip Chaney
                                        </td>
                                        <td>
                                            Korea, South
                                        </td>
                                        <td>
                                            Overland Park
                                        </td>
                                        <td class="text-right">
                                            $38,735
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Doris Greene
                                        </td>
                                        <td>
                                            Malawi
                                        </td>
                                        <td>
                                            Feldkirchen in Kärnten
                                        </td>
                                        <td class="text-right">
                                            $63,542
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mason Porter
                                        </td>
                                        <td>
                                            Chile
                                        </td>
                                        <td>
                                            Gloucester
                                        </td>
                                        <td class="text-right">
                                            $78,615
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Jon Porter
                                        </td>
                                        <td>
                                            Portugal
                                        </td>
                                        <td>
                                            Gloucester
                                        </td>
                                        <td class="text-right">
                                            $98,615
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
@endsection