<nav class="navbar navbar-expand-lg" color-on-scroll="500">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">  </a>
        <button href="" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar burger-lines"></span>
            <span class="navbar-toggler-bar burger-lines"></span>
            <span class="navbar-toggler-bar burger-lines"></span>
        </button>
        <!-- <div class="navbar-collapse justify-content-end" id="navbarSupportedContent"> -->
        <div class="justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav d-flex align-items-center">
                <li class="nav-item d-flex justify-content-center">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <a class="text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Log out') }} 
                        </a>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>