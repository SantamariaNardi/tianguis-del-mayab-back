<div class="sidebar" data-image="assets/img/sidebar-5.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                {{ __("Tianguis Del Mayab") }}
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item @if($activePage == 'dashboard') active @endif">
                <a class="nav-link" href="{{route('dashboard')}}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>{{ __("About Us") }}</p>
                </a>
            </li>
            <li class="nav-item @if($activePage == 'user-management') active @endif">
                <a class="nav-link" href="{{ route('users.index') }}">
                    <i class="nc-icon nc-circle-09"></i>
                    <p>{{ __("Ver usuarios") }}</p>
                </a>
            </li>
            <li class="nav-item @if($activePage == 'categories') active @endif">
                <a class="nav-link" href="{{ route('categories.index') }}">
                    <i class="nc-icon nc-grid-45"></i>
                    <p>{{ __("Categorias") }}</p>
                </a>
            </li>
            <li class="nav-item @if($activePage == 'producers') active @endif">
                <a class="nav-link" href="{{ route('producers.index') }}">
                    <i class="nc-icon nc-badge"></i>
                    <p>{{ __("Productores") }}</p>
                </a>
            </li>
            <li class="nav-item @if($activePage == 'products') active @endif">
                <a class="nav-link" href="{{ route('products.index') }}">
                    <i class="nc-icon nc-app"></i>
                    <p>{{ __("Productos") }}</p>
                </a>
            </li>
            <!-- <li class="nav-item @if($activePage == 'maps') active @endif">
                <a class="nav-link" href="">
                    <i class="nc-icon nc-pin-3"></i>
                    <p>{{ __("Maps") }}</p>
                </a>
            </li> -->
            <li class="nav-item @if($activePage == 'events') active @endif">
                <a class="nav-link" href="{{ route('events.index') }}">
                    <i class="nc-icon nc-bell-55"></i>
                    <p>{{ __("Eventos") }}</p>
                </a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link active bg-danger" href="">
                    <i class="nc-icon nc-alien-33"></i>
                    <p>{{ __("Upgrade to PRO") }}</p>
                </a>
            </li> -->
        </ul>
    </div>
</div>
