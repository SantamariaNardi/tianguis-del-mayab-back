<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('history')->nullable();
            $table->string('img');
            $table->string('code');
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('status', [0, 1])->default(1);
            $table->date('initial_date');
            $table->date('end_date')->nullable();
            $table->string('facebook_link');
            $table->string('whatsapp_link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producers');
    }
}
